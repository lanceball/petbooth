'use strict';

const config = require('./config');

const express = require('express');
const fs = require('fs');
const app = express();

const { takeASelfie } = require('./services/raspicam');
const { sendText, createTwimlResponse } = require('./services/twilio');
const { getLocalTimestamp, log, snooze, toNiceISOString } = require('./services/utils');
const treatDispenser = require('./services/treatDispenser');

app.use(express.urlencoded({ extended: false }));

// Redirect http to https but leave local requests (e.g. http://petbooth.local:8080) as-is
app.use(function(req, res, next) {
  if(!req.secure && req.get('Host') != undefined && req.get('Host').toLowerCase() == config.app.host) {
    return res.redirect(['https://', req.get('Host'), req.url].join(''));
  }
  next();
});

app.use(express.static('static'));
app.use('/images', express.static('/home/pi/petbooth/images'));

app.get('/', (req, res) => {
  res.send('<html><head><link rel="stylesheet" href="petbooth.css" /><title>Petbooth</title></head><body><form action="/selfie" method="GET"><p>Welcome to Petbooth!</p><p>Enter your phone number to give the pets a treat and get a selfie back!:<br/><input type="text" name="number"/><input type="submit" value="Go!"/></form></body></html>');
});

app.get('/selfie', async (req, res) => {
  if(req.query.authorized == 'y') {
    const response = await doStuff(getTargetNumber(req));
    log('Sending response to HTTP selfie request');
    res.send(response);
  } else {
    res.send('<html><head><link rel="stylesheet" href="petbooth.css" /></head><body><h1>Nope!</h1><body>The pets are very full of treats now. Sorry but they need a break.</body></html>');
  }
});

app.get('/selfie-only', async (req, res) => {
  const selfieUrl = await takeASelfie()
  log('Sending response to HTTP selfie-only request');
  res.send(`<html><head><link rel="stylesheet" href="petbooth.css" /></head><body>Just a selfie:<br/><a href="${selfieUrl}">
    <img src="${selfieUrl}" width="820" height="616"/></a></body></html>`);
});

app.get('/send-sms', async (req, res) => {
  const now = toNiceISOString(getLocalTimestamp());
  const body = `Greetings! The current time is: ${now}`;
  const sendTo = getTargetNumber(req);
  log(`Ready to send message "${body}"`);

  const twilioMessage = await sendText(config.twilio.phoneNumber, sendTo, body);

  const output = `Message "${body}" sent to ${sendTo} at ${toNiceISOString(getLocalTimestamp())}. (ID: ${twilioMessage.sid})`;
  log(output);
  res.send(output);
});

app.post('/twilio-callback', async (req, res) => {
  console.log('Received POST at /twilio-callback');
  console.log(JSON.stringify(req.body));

  const MessagingResponse = require('twilio').twiml.MessagingResponse
  const incomingText = req.body.Body.toLowerCase();
  const msgWords = incomingText.split(' ');
  const cmd = msgWords[0];

  const response = new MessagingResponse();
  const message = response.message();
  const eventMap = {
    '?': async () => {
      message.body('You can send any of the following requests:\nSelfie\nStealth');
    },
    'selfie': async () => {
      if(msgWords.indexOf('please') == -1) {
        message.body('You didn\'t say please. ☹️');
        return;
      }
      await callThePets();
      const selfieUrl = await takeASelfie();

      // Distributing treats and sending message can run in parallel
      // And we don't have to wait for them to complete to send response

      treatDispenser.distributeTreats();
      message.body('Thanks for the treats! Enjoy the selfie!');
      message.media(selfieUrl);
    },
    'stealth': async () => {
      const selfieUrl = await takeASelfie()
      message.body('Shhhh! Here\'s the kitchen right now! Nobody knows we took this picture.');
      message.media(selfieUrl);
    }
  };
  eventMap[cmd] ? await eventMap[cmd]() : message.body(`I have no idea what you mean by "${cmd}". Ask for help if you need it.`);
  res.writeHead(200, {'Content-Type': 'text/xml'});
  console.log(response.toString());
  res.end(response.toString());
});

app.get('/servo', async (req, res) => {
  try {
    await treatDispenser.distributeTreats();
  }
  catch(e) {
    log(`ERROR: ${e}`);
  }
  res.send('I hopefully moved the servo');
});

app.get('/call-the-pets', async (req, res) => {
  try {
    await callThePets();
  }
  catch(e) {
    log(`ERROR: ${e}`);
  }
  res.send('I hopefully played a sound');
});

app.get('/sound-and-furry', async (req, res) => {
  await callThePets();
  treatDispenser.distributeTreats().catch((e) => { log(`ERROR: ${e}!`)});
  res.send('I called \'em and fed \'em');
});

app.use(function (err, req, res, next) {
  log(`UNHANDLED ERROR: ${err}`);
  res.status(500).render('error', { error: err });
});

const doStuff = async (smsAddress) => {
  // Must call the pets and THEN take a picture
  await callThePets();
  const selfieUrl = await takeASelfie();

  // Distributing treats and sending message can run in parallel
  // And we don't have to wait for them to complete to send response
  treatDispenser.distributeTreats();
  sendSelfie(selfieUrl, smsAddress);

  log('Started distributing treats and sending selfie. Not waiting for them to complete though.');
  return `<html><head><link rel="stylesheet" href="petbooth.css" /></head><body>Thanks for the treats!!<br/><a href="${selfieUrl}">
    <img src="${selfieUrl}" width="820" height="616"/></a></body></html>`;
};

const getTargetNumber = (req) => {
  return req.query.number || config.twilio.myNumber;
}

const callThePets = async () => {
  log('Calling the pets');
  const petCaller = require('./services/petCaller');
  await petCaller.callPets('/home/pi/petbooth/sounds/charge.wav');
  await snooze(4000);
  log('Pets have been called');
  return;
};

const sendSelfie = async (selfieUrl, sendTo) => {
  const body = 'Thanks for the treats!';
  const mediaUrl = `${config.app.publicUrl}${selfieUrl}`;

  log(`Sending selfie: ${mediaUrl} . . .`);
  const twilioMessage = await sendText(config.twilio.phoneNumber, sendTo, body, mediaUrl);
  const output = `Message "${body}" sent to ${sendTo} with selfie ${mediaUrl} at ${toNiceISOString(getLocalTimestamp())}. (ID: ${twilioMessage.sid})`;
  log(output);
};

let server = null;

app.listen(config.app.httpPort, () => {
   const https = require('https');
   const options = {
    cert: fs.readFileSync('./sslcert/fullchain.pem'),
    key: fs.readFileSync('./sslcert/privkey.pem')
   };
   server = https.createServer(options, app).listen(config.app.httpsPort);

   require('./services/petCaller').callPets('/home/pi/petbooth/sounds/startup.wav');

   // Assumes router forwards standard port 443 to app port configured above
   // And external requests will all be https, internal will all be http
   log(`Petbooth listening at
   internal - http://${require('os').hostname()}:${config.app.httpPort}
   external - https://${config.app.host}`);
});

const shutdown = (signal) => {
  log(`${signal} signal received. Shutting down after current requests complete . . .`);
  server.close(function () {
    require('./services/petCaller').callPets('/home/pi/petbooth/sounds/shutdown.wav')
    .then(() => snooze(1000))
    .then(() =>
    {
      log('Adios!');
      process.exit(0);
    });
  });
}

['SIGHUP', 'SIGINT', 'SIGTERM', 'SIGCONT'].map(
    signal => { process.on(signal, () => shutdown(signal)) });
