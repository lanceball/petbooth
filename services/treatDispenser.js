const { log, snooze } = require('./utils');
const servo = require('./servo');

const distributeTreats = async () => {
  log('Start distributing treats');
  try {
    for(let x=0; x < 10; x++) {
      await servo.raw(1600)
      await snooze(100);
      await servo.raw(1425);
      await snooze(100);    
    }
    await servo.moveFor(servo.direction.clockwise, 6, 1000);
    log('End distributing treats');
    return;
  }
  catch(e) {
    log(`ERROR distributing treats: ${e}. Stopping servo and aborting.`);
    servo.stop();
  }
}

module.exports = {
  distributeTreats: distributeTreats,
  shutdown: () => { log('Calling servo.terminate()'); servo.terminate(); }
}
