const config = require('../config');
const spawn = require('await-spawn');
const { getUniqueFilename, log } = require('./utils.js');
let cameraInUse = 0;

const takeASelfie = async () => {
  if(cameraInUse > 0) {
    throw 'Camera is already in use';
  }
  try {
    cameraInUse++;
    log('Start taking picture');
    const filename = `${getUniqueFilename()}.jpg`;
    const imagePath = `${config.app.imagePath}/${filename}`;
    log(`Picture will be saved to ${imagePath}`);
    const cameraOutput = await spawn('raspistill', `-t 3000 -br 70 -co 40 -w 800 -h 600 -ex auto -o ${imagePath}`.split(' '));
    if(cameraOutput.length) {
      log(`UNEXPECTED RESPONSE FROM RASPISTILL: ${cameraOutput}`);
    } else {
      log(`Picture has been saved to ${imagePath}`);
    }
    return imagePath;
  }
  catch(e) {
    log(`ERROR taking selfie. Message: ${e.message}, Stack: ${e.stack}\nReturning placeholder error image instead.`);
    return `${config.app.imagePath}/errorstop.png`;
  }
  finally {
    cameraInUse -= 1;
  }
};

module.exports = { takeASelfie: takeASelfie };
