const player = require('play-sound')();
const { log } = require('./utils.js');

const callPets = async (filename) => {
  filename = filename || './sounds/charge.wav';
  log(`Trying to play file ${filename}`)
  return player.play(filename, function(err) { 
    if(err) {
      log(`ERROR playing sound: ${err}`);
    }
  });
}

module.exports = {
  callPets: callPets
}