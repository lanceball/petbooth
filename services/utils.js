const getLocalTimestamp = () => {
  const tempUtcTimestamp = new Date();
  const tzOffsetMinutes = tempUtcTimestamp.getTimezoneOffset();
  return new Date(tempUtcTimestamp.getTime() - tzOffsetMinutes * 60000);
};

const getUniqueFilename = (() => {
  let lastTimeString = '';
  let lastCounter = 0;
  return (() => {
    const localTimeString = toNiceISOString(getLocalTimestamp()).replace(/:/g, '');
    lastCounter = (localTimeString == lastTimeString) ? ++lastCounter : 0;
    lastTimeString = localTimeString;
    return localTimeString + (lastCounter == 0 ? '' : `-${lastCounter}`);
  });
})();

const log = (msg) => {
  console.log(getLocalTimestamp().toISOString().slice(0,23) + ' - ' + msg);
};

// TODO: Temporary to make stub methods take real time to complete
const snooze = ms => new Promise(resolve => setTimeout(resolve, ms));

const toNiceISOString = (date) => {
  return (date.toISOString().slice(0, 19));
}

module.exports = { 
  getLocalTimestamp: getLocalTimestamp,
  getUniqueFilename: getUniqueFilename,
  log: log,
  snooze: snooze,
  toNiceISOString: toNiceISOString
};


