echo Starting petbooth . . .
if [ "$HOSTNAME" = petbooth ]; then
    sudo systemctl start petbooth
else
    ssh petbooth sudo systemctl start petbooth
fi
echo Done
