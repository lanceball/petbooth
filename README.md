# *Documentation is _super_ under construction! Come back soon for updates!*

# What
A selfie station for your pets!

1. You send them a selfie request over SMS
1. Twilio calls this app
1. It plays music to call your pets over
1. It (hopefully) takes a picture of them in front of the device
1. It dispenses treats and texts you the picture back.

# About this repo
The file `config.js` is protected by [transcrypt](https://github.com/elasticdog/transcrypt), allowing it to be safely checked into source control. Since this prevents you from reading mine and you probably want to create your own anyway (and prevent it from giving up secrets when checked into source control), you should do the following:

1. [Install transcrypt](https://github.com/elasticdog/transcrypt/blob/master/INSTALL.md)
1. Copy *config.js.template* to *config.js*
1. Follow [Transcrypt's instructions](https://github.com/elasticdog/transcrypt) for configuring your repository and ensuring git is encrypting your `config.js` before committing it to source control.

# Parts
* Raspberry Pi (2 or 3, or probably Zero WH)
* Continuous rotation servo (I used [SM-S4303R](http://tinkersphere.com/raspberry-pi-compatible-components/956-springrc-sm-s4303r-continuous-rotation-servo.html))
* Powered speaker
* Micro USB cable to power Pi, w/ jumper wires spliced in to power servo
* Wood
* 1.5" PVC ball valve

# Wiring

## Raspberry Pi GPIO

Servo will take signal from Raspberry Pi. Default pin for [pulse width modulation](http://rpi.science.uoit.ca/lab/servo/) is GPIO pin 18, but hardware PWM is also used for audio output, which we need, so we have to do PWM in software. This is less accurate and more CPU-intensive, but we don't really care about either concern. It'll be fine!

![GPIO Pins](https://www.raspberrypi.org/documentation/usage/gpio/images/gpio-numbers-pi2.png)

* 5 - Servo control (but any GPIO pin is fine. Just update the code accordingly.)
* Any ground pin - Connect Pi ground to servo ground to ensure that any wiring mistakes don't fry the Pi or the servo or both.

## Servo
The servo will get its power from the jumper wires spliced into the micro USB cable powering the Pi

## Speaker
I used a speaker with a standard 1/8" audio cable into the Pi's headphone jack for the source signal and a mini USB cable into one of the Pi's USB ports for power.